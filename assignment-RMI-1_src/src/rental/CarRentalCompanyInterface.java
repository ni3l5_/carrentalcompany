package rental;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface CarRentalCompanyInterface extends Remote {

    String getName() throws RemoteException;

    List<String> getRegions() throws RemoteException;

    boolean hasRegion(String region) throws RemoteException;

    Collection<CarType> getAllCarTypes() throws RemoteException;

    CarType getCarType(String carTypeName) throws RemoteException;

    boolean isAvailable(String carTypeName, Date start, Date end) throws RemoteException;

    /* client need */
    Set<CarType> getAvailableCarTypes(Date start, Date end) throws RemoteException;

    int getNumberOfReservationsByCarType(String ctype) throws RemoteException;

    Quote createQuote(ReservationConstraints constraints, String client) throws ReservationException, RemoteException;

    Reservation confirmQuote(Quote quote) throws ReservationException, RemoteException;

    List<Reservation> getAllReservationsByRenter(String clientName) throws RemoteException;

    void cancelReservation(Reservation res) throws ReservationException, RemoteException;
}
